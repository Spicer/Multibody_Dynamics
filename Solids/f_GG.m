function [G] = f_GG(nbd,Y)

G = [];

for body = 1:nbd
    G(3*body-2:3*body,4*body-3:4*body)=f_G(nbd,body,Y);
end

end

