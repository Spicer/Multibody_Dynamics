function [A] = f_A(nbd,body,Y)

[~,~,~,p] = f_StateVar(Y,nbd);

e = f_e(p, body);

e0 = e(1);
e1 = e(2);
e2 = e(3);
e3 = e(4);

A=2*[e0^2+e1^2-0.5 e1*e2-e0*e3 e1*e3+e0*e2;...
    e1*e2+e0*e3 e0^2+e2^2-0.5 e2*e3-e0*e1;...
    e1*e3-e0*e2 e2*e3+e0*e1 e0^2+e3^2-0.5;];
end

