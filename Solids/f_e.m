function e = f_e(p, body)

if body == 0
    e0 = 1;
    e1 = 0;
    e2 = 0;
    e3 = 0;
else
    e0 = p(body*4-3:body*4);
    e1 = p(body*4-3:body*4);
    e2 = p(body*4-3:body*4);
    e3 = p(body*4-3:body*4);
end

e = [e0; e1; e2; e3];

end
