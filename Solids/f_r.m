function rA=f_r(Y,nbd,body,sprimeA)

[~,~,r,~]=f_StateVar(Y,nbd);

if body~=0
    [x,y,z]=f_cg(r,body);
    r=[x y z]';
    A=f_A(nbd,body,Y);
    
else 
    r=zeros(3,1);
    A=eye(3);
end

    rA=r+A*sprimeA; 
end
