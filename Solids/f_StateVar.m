function [qr,qp,r,p] = f_StateVar(Y,nbd)

qr = Y(1:nbd*3);
qp = Y(nbd*3+1:nbd*7);
r = Y(nbd*7+1: nbd*10);
p = Y(nbd*10+1:end);

end

