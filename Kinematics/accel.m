function [qs,Acc]=accel(t,Y,nbd,nd,PHIparameters)

Acc=  f_Gamma(t,Y,nbd,nd,PHIparameters);

PHIq= jacobian(Y,nbd,nd,PHIparameters);
        
qs= PHIq\Acc;

end