function [qv,PHI_t]=velocity(t,Y,nbd,nd,PHIparameters,No_of_Kinematic_const)

PHI_t_k=zeros(No_of_Kinematic_const,1);
PHI_t_sper=zeros(3,1);
PHI_t_d= f_PHI_t_d(t);
PHI_t_norm=zeros(5,1);

PHI_t_dr_a=[-0.5*sin(t/2); 0; 0.5*cos(t/2); 0;];

PHI_t=[PHI_t_k;PHI_t_sper;PHI_t_d;PHI_t_norm;PHI_t_dr_a;];

PHI_q=jacobian(Y,nbd,nd,PHIparameters);
  
qv= PHI_q\PHI_t;

end