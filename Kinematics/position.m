function [Y,k] = position(t,Y,nbd,nd,PHIparameters)

[~,~,r,p]=f_StateVar(Y,nbd);
q=[r;p];

k = 0;

while (k==0 || norm(PHI)>1e-4)
    
    PHI = Evalconstraints(t,Y,nbd,nd,PHIparameters);
    
    norm(PHI);
   
    PHIq = jacobian(Y,nbd,nd,PHIparameters);
    
    delta_q=-PHIq\PHI;
   
    delta_r=zeros(18,1);
    delta_p=zeros(24,1);
    
    for body=1:nbd
       delta_r(body*3-2:body*3)=delta_q(body*7-6:body*7-4);
    end
    
    for body=1:nbd
       delta_p(body*4-3:body*4)=delta_q(body*7-3:body*7);
    end
    
    delta_new=[delta_r;delta_p];
    
    q=q+delta_new; 
    k=k+1; 
    
    Y=[zeros(nbd*7,1);q];
    
    
end


end

