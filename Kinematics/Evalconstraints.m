function [PHI] = Evalconstraints(t,Y,nbd,nd,PHIparameters)

body1 = PHIparameters.sp_constraint(1).body1;
body2 = PHIparameters.sp_constraint(1).body2;
sprime1 = PHIparameters.sp_constraint(1).sprime1;
sprime2 = PHIparameters.sp_constraint(1).sprime2;

contraint_spherical = f_PHI_spherical(Y,nbd,body1,body2,sprime1,sprime2);

for i = 1:nbd
    body1 = PHIparameters.rev_constraint(i).body1;
    body2 = PHIparameters.rev_constraint(i).body2;
    sprime1 = PHIparameters.rev_constraint(i).sprime1;
    sprime2 = PHIparameters.rev_constraint(i).sprime2;
    con_p = f_PHI_parallel(Y,nbd,body1,body2);
    con_s = f_PHI_spherical(Y,nbd,body1,body2,sprime1,sprime2);
    constraint{i} = [con_p;con_s];
end
constraint_revolute = [];
for i = 1:nbd
    constraint_revolute = [constraint_revolute;constraint{i}];
end
con_r1 = constraint_revolute(1:10,:);

con_r2 = constraint_revolute(13:26,:);

con_r3 = constraint_revolute(28:30,:);

constraint_revolute = [con_r1;con_r2;con_r3];
for i = 1:nd
    s = f_s(i,t);
    body1 = PHIparameters.dr_constraint(i).body1;
    body2 = PHIparameters.dr_constraint(i).body2;
    sprime1 = PHIparameters.dr_constraint(i).sprime1;
    sprime2 = PHIparameters.dr_constraint(i).sprime2;
    constraint_driving{i} = f_PHI_drivingdistance(Y,s,nbd,body1,body2,sprime1,sprime2);
end
constraint_distancedriving = [];
for i = 1:nd
    constraint_distancedriving = [constraint_distancedriving;constraint_driving{i}];
end

constraint_angular = f_driving_angle(t,Y,nbd);

constraintp = f_PHI_angle(Y,nbd);

PHI = [constraint_revolute;contraint_spherical;constraint_distancedriving;constraintp;constraint_angular];
end

