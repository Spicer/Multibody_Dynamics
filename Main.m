% function [time,Q_ddot,Q_dot,Q, Phi] = Main(constraints, n, nd, s_primes, r0, q0)
clc
clear
close all

addpath(genpath('Solids'));         % adding the path to the 'Solids' Folder
addpath(genpath('Jacobian'));       % adding the path to the 'Jacobian' Folder
addpath(genpath('Constraints'));    % adding the path to the 'Constraints' Folder
addpath(genpath('Gamma'));          % adding the path to the 'Gamma' Folder
addpath(genpath('Kinematics'));

%%

% nb = 6;
nd = 3;
n = 42;
nb = n / 7;

%% Sprime Definations

% Ground
sprimeO0 = [0; 0; 0];

% Body 1
sprimeF1 = [0.322; 1.338; 0];
sprimeO1 = [0; 0; 0];
sprimeP1 = [0.792; 0.980; 0];

% Body 2
sprimeF2 = [-2.545; 0.296; 0];
sprimeQ2 = [-0.445; 0.292; 0];
sprimeR2 = [1.027; 0.079; 0];
sprimeA2 = [2.433; -1.470; 0];

% Body 3
sprimeS3 = [-1.393; 0.142; 0];
sprimeA3 = [-0.863; -0.088; 0];
sprimeT3 = [-0.787; 0.401; 0];
sprimeC3 = [1.330; -0.058; 0];
sprimeB3 = [1.690; -0.058; 0];

% Body 4
sprimeB4 = [-0.766; -0.440; 0];
sprimeE4 = [-0.754; -0.059; 0];

% Body 5
sprimeC5 = [-0.589/2; 0; 0];
sprimeD5 = [0.589/2; 0; 0];

% Body 6
sprimeD6 = [-0.589/2; 0; 0];
sprimeE6 = [0.589/2; 0; 0];


%% Initial Guess
q_cabin  = [0 0 0 1 0 0 0];
q_boom   = [1.2 3.2 0 cos(pi/8) 0 0 sin(pi/8)];
q_stick  = [5 3.3 3 cos(pi/8) 0 0 sin(pi/8)];
q_bucket = [6 4 0 cos(pi/4) 0 0 sin(pi/4)];
q_rod1   = [6 0 0 cos(pi/4) 0 0 sin(pi/4)];
q_rod2   = [6 0 0 0.94 0 0 -0.32];

r0 = [q_cabin(1:3) q_boom(1:3) q_stick(1:3) q_bucket(1:3) q_rod1(1:3) q_rod2(1:3)]';
p0 = [q_cabin(4:end) q_boom(4:end) q_stick(4:end) q_bucket(4:end) q_rod1(4:end) q_rod2(4:end)]';


qr0 = zeros(length(r0),1);
qp0 = zeros(length(p0),1);

Y = [qr0; qp0; r0; p0];

%% Joint structures

PHIparameters.rev_constraint(1).body1 = 1;
PHIparameters.rev_constraint(1).body2 = 2;
PHIparameters.rev_constraint(1).sprime1 = sprimeF1;
PHIparameters.rev_constraint(1).sprime2 = sprimeF2;

PHIparameters.rev_constraint(2).body1 = 2;
PHIparameters.rev_constraint(2).body2 = 3;
PHIparameters.rev_constraint(2).sprime1 = sprimeA2;
PHIparameters.rev_constraint(2).sprime2 = sprimeA3;

PHIparameters.rev_constraint(3).body1 = 3;
PHIparameters.rev_constraint(3).body2 = 4;
PHIparameters.rev_constraint(3).sprime1 = sprimeB3;
PHIparameters.rev_constraint(3).sprime2 = sprimeB4;

PHIparameters.rev_constraint(4).body1 = 3;
PHIparameters.rev_constraint(4).body2 = 5;
PHIparameters.rev_constraint(4).sprime1 = sprimeC3;
PHIparameters.rev_constraint(4).sprime2 = sprimeC5;

PHIparameters.rev_constraint(5).body1 = 5;
PHIparameters.rev_constraint(5).body2 = 6;
PHIparameters.rev_constraint(5).sprime1 = sprimeD5;
PHIparameters.rev_constraint(5).sprime2 = sprimeD6;

PHIparameters.rev_constraint(6).body1 = 4;
PHIparameters.rev_constraint(6).body2 = 6;
PHIparameters.rev_constraint(6).sprime1 = sprimeE4;
PHIparameters.rev_constraint(6).sprime2 = sprimeE6;

PHIparameters.sp_constraint(1).body1 = 0;
PHIparameters.sp_constraint(1).body2 = 1;
PHIparameters.sp_constraint(1).sprime1 = sprimeO0;
PHIparameters.sp_constraint(1).sprime2 = sprimeO1;

%% Driving Constraints

PHIparameters.dr_constraint(1).body1 = 1;
PHIparameters.dr_constraint(1).body2 = 2;
PHIparameters.dr_constraint(1).sprime1 = sprimeP1;
PHIparameters.dr_constraint(1).sprime2 = sprimeQ2;

PHIparameters.dr_constraint(2).body1 = 2;
PHIparameters.dr_constraint(2).body2 = 3;
PHIparameters.dr_constraint(2).sprime1 = sprimeR2;
PHIparameters.dr_constraint(2).sprime2 = sprimeS3;

PHIparameters.dr_constraint(3).body1 = 3;
PHIparameters.dr_constraint(3).body2 = 5;
PHIparameters.dr_constraint(3).sprime1 = sprimeT3;
PHIparameters.dr_constraint(3).sprime2 = sprimeD5;

PHIparameters.ad_constraint(1).body1 = 0;
PHIparameters.ad_constraint(1).body2 = 1;
PHIparameters.ad_constraint(1).sprime1 = sprimeO0;
PHIparameters.ad_constraint(1).sprime2 = sprimeO1;

No_of_Kinematic_const=27;

%% 

i = 1;

t_step=0.01;
t_final=1;
time=0:t_step:t_final;

Q=zeros(nb*7,(t_final/t_step)+1);
Q_dot=zeros(nb*7,(t_final/t_step)+1);
Q_ddot=zeros(nb*7,(t_final/t_step)+1);

for t=0:t_step:t_final;
    
    Y = position(t,Y,nb,nd,PHIparameters);
    [~,~,r,p]=f_StateVar(Y,nb);
    q=[r;p];
    Q(:,i)=q;
    
    [qv,vequation]=velocity(t,Y,nb,nd,PHIparameters,No_of_Kinematic_const);
    qv_r=zeros(18,1);
    qv_p=zeros(24,1);
    for body=1:nb
       qv_r(body*3-2:body*3)=qv(body*7-6:body*7-4);
    end
    for body=1:nb
       qv_p(body*4-3:body*4)=qv(body*7-3:body*7);
    end
    qv_new=[qv_r;qv_p];
    Q_dot(:,i)=qv_new;        
    Y=[qv_new;q];
    
    [qa,Acc]=accel(t,Y,nb,nd,PHIparameters);
    qa_r=zeros(18,1);
    qa_p=zeros(24,1);
    for body=1:nb
       qa_r(body*3-2:body*3)=qa(body*7-6:body*7-4);
    end
    for body=1:nb
       qa_p(body*4-3:body*4)=qa(body*7-3:body*7);
    end
    qa_new=[qa_r;qa_p];
    
    Q_ddot(:,i)=qa_new;
    q=q+0.01*qv_new+0.5*0.0001*qa_new;
    
    i=i+1;
end

disp('working');

%% Euler Parameter

figure(1);
plot(time,Q(19:22,:));
grid on;
xlabel('Time (sec)');
ylabel('Euler parameters (p) of the cabin');
legend('e0','e1','e2','e3');

figure(2);
plot(time,Q_dot(19:22,:));
grid on;
h=legend('$\dot{e_0}$ of Body-1','$\dot{e_1}$ of Body-1','$\dot{e_2}$ of Body-1','$\dot{e_3}$ of Body-1');
set(h,'Interpreter','latex');
title('$\dot{p}$ of Body-1','Interpreter', 'latex')
xlabel('Time, $\it t \rm [s]$','Interpreter', 'latex')
ylabel('$\dot{p}$','Interpreter', 'latex')


figure(3);
plot(time,Q_ddot(19:22,:));
grid on;
h=legend('$\ddot{e_0}$ of Body-1','$\ddot{e_1}$ of Body-1','$\ddot{e_2}$ of Body-1','$\ddot{e_3}$ of Body-1');
set(h,'Interpreter','latex');
title('$\ddot{p}$ of Body-1','Interpreter', 'latex')
xlabel('Time, $\it t \rm [s]$','Interpreter', 'latex')
ylabel('$\ddot{p}$','Interpreter', 'latex')

%% Plots
figure(4);
plot(time,Q(1:3,:),time,Q(4:6,:),time,Q(7:9,:),time,Q(10:12,:),time,Q(13:15,:),time,Q(16:18,:));
grid on;
xlabel('Time (sec)');
ylabel('Positions of the Bodies');
h=legend(' $x_1$',' $y_1$','$z_1$',' $x_2$',' $y_2$','$z_2$',' $x_3$',' $y_3$','$z_3$',' $x_4$',' $y_4$','$z_4$',' $x_5$',' $y_5$','$z_5$',' $x_6$',' $y_6$','$z_6$');
set(h,'Interpreter', 'latex');


figure(5);
plot(time,Q_dot(1:3,:),time,Q_dot(4:6,:),time,Q_dot(7:9,:),time,Q_dot(10:12,:),time,Q_dot(13:15,:),time,Q_dot(16:18,:));
grid on;
xlabel('Time (sec)');
ylabel('Velocities of Bodies');
h=legend('$\dot{x_1}$',' $\dot{y_1}$','$\dot{z_1}$','$\dot{x_2}$',' $\dot{y_2}$','$\dot{z_2}$','$\dot{x_3}$',' $\dot{y_3}$','$\dot{z_3}$','$\dot{x_4}$',' $\dot{y_4}$','$\dot{z_4}$','$\dot{x_5}$',' $\dot{y_5}$','$\dot{z_5}$','$\dot{x_6}$',' $\dot{y_6}$','$\dot{z_6}$');
set(h,'Interpreter', 'latex');

figure(6);
plot(time,Q_ddot(1:3,:),time,Q_ddot(4:6,:),time,Q_ddot(7:9,:),time,Q_ddot(10:12,:),time,Q_ddot(13:15,:),time,Q_ddot(16:18,:));
grid on;
xlabel('Time (sec)');
ylabel('Accelerations of Bodies');
h=legend('$\ddot{x_1}$',' $\ddot{y_1}$','$\ddot{z_1}$','$\ddot{x_2}$',' $\ddot{y_2}$','$\ddot{z_2}$','$\ddot{x_3}$',' $\ddot{y_3}$','$\ddot{z_3}$','$\ddot{x_4}$',' $\ddot{y_4}$','$\ddot{z_4}$','$\ddot{x_5}$',' $\ddot{y_5}$','$\ddot{z_5}$','$\ddot{x_6}$',' $\ddot{y_6}$','$\ddot{z_6}$');
set(h,'Interpreter', 'latex');
% end