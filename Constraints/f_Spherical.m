function [PHI_sp]=f_Spherical(Y,nbd,nd,body1,body2,sprime1,sprime2)



if body1~=0 && body2~=0 % If none of the bodies are ground
    %Position of the joint in global reference frame using its position in local reference frame of each bodies
    [rA1]=f_r(Y,nbd,body1,sprime1); 
    [rA2]=f_r(Y,nbd,body2,sprime2); 
    
    PHI_sp=rA2-rA1; 

else
    if body1==0 % If one of the bodies is connected to ground using revolute joint 
        [rA2]=f_r(q,body2,sprime2); 
        rA1=sprime1;
        PHI_sp=rA2-rA1;
    else
        error('wrong configuration')
    end
end

end