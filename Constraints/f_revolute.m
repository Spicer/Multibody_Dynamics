function [phi] = f_revolute(Y,body1,sprimeA1,body2,sprimeA2,n,m)

[~,~,q] = f_StateVar(Y,n,m);

if body1~=0 && body2~=0
    [rA1]=f_r(q,body1,sprimeA1);
    [rA2]=f_r(q,body2,sprimeA2);
else
    if body1==0
        [rA2]=f_r(q,body2,sprimeA2);
        rA1=[0 0]';
    else
        error('wrong configuration')
    end
end
phi=rA1-rA2;
end

