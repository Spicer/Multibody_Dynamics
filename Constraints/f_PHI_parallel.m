function [con_p] = f_PHI_parallel(Y,nbd,body1,body2)

fi = [1;0;0];
gi = [0;1;0];
hj = [0;0;1];

if body1~=0 && body2~=0
    Ai = f_A(nbd,body1,Y);
    Aj = f_A(nbd,body2,Y);
else
    if body1 == 0
        Ai = eye(3);
        Aj = f_A(nbd,body2,Y);
    else
        error('wrong configuration')
    end
end
con_d11 = transpose(fi)*transpose(Ai)*Aj*hj;
con_d12 = transpose(gi)*transpose(Ai)*Aj*hj;
con_p = [con_d11;con_d12];
end

