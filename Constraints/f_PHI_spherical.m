function [con_sp] = f_PHI_spherical(Y,nbd,body1,body2,sprime1,sprime2)

[~,~,r,~] = f_StateVar(Y,nbd);
if body1~=0 && body2~=0
    Ai = f_A(nbd,body1,Y);
    Aj = f_A(nbd,body2,Y);
    ri = r(body1*3-2:body1*3);
    rj = r(body2*3-2:body2*3);
else
    if body1 == 0
        Ai = eye(3);
        Aj = f_A(nbd,body2,Y);
        ri = zeros(3,1);
        rj = r(body2*3-2:body2*3);
    else
        error('wrong configuration')
    end
end
con_sp = rj + Aj*sprime2 - ri - Ai*sprime1;


end

