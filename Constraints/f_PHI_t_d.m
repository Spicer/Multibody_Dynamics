function PHI_t_d=f_PHI_t_d(t)

PHI_t_d=[-0.5; 4.5*cos(10*t); 3*cos(10*t);];

end