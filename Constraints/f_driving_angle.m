function [con] = f_driving_angle(t,Y,nbd)

[~,~,~,p] = f_StateVar(Y,nbd);

pi = p(1*4-3:1*4);

con_1 = pi(1)-cos(t/2);
con_2 = pi(2);
con_3 = pi(3) - sin(t/2);
con_4=  pi(4);

con = [con_1;con_2;con_3;con_4];
end

