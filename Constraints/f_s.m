function [s] = f_s(i,t)

C = [2.35 - 0.5*t;...
    2.10 + 0.45*sin(10*t);...
    2.00 + 0.3*sin(10*t)];

s = C(i);

end

