function [con_ep] = f_con_ep(Y,nbd)

[~,~,~,p] = f_StateVar(Y,nbd);
for i = 1:nbd
    pi = p(i*4-3:i*4);
    con_epi{i} = transpose(pi) * pi - 1;
end
con_ep = [];
for i = 2:nbd
    con_ep = [con_ep;con_epi{i}];
end
end

