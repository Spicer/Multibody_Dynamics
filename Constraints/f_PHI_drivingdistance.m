function [con_dr_dis] = f_PHI_drivingdistance(Y,s,nbd,body1,body2,sprime1,sprime2)

[~,~,r,~] = f_StateVar(Y,nbd);
if body1~=0 && body2~=0
    Ai = f_A(nbd,body1,Y);
    Aj = f_A(nbd,body2,Y);
    ri = r(body1*3-2:body1*3);
    rj = r(body2*3-2:body2*3);
else
    if body1 == 0
        Ai = eye(3);
        Aj = f_A(nbd,body2,Y);
        ri = zeros(3,1);
        rj = r(body2*3-2:body2*3);
    else
        error('wrong configuration')
    end
end
d = rj + Aj*sprime2 - ri - Ai*sprime1;

con_dr_dis = transpose(d)*d - s^2;
end

