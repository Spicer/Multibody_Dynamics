function[gamma_dr_d]=f_gamma_dr_d(Y,nbd,nd,PHIparameters,t)

dc=dct(t);
dc_dot=dct_dot(t);
dc_dotdot=dct_dotdot(t);

gamma_dr_d=zeros(3,1);

for j=1:nd
    
    body1=PHIparameters.dr_constraint(j).body1;
    body2=PHIparameters.dr_constraint(j).body2;
    
    sprime1= PHIparameters.dr_constraint(j).sprime1;
    sprime2= PHIparameters.dr_constraint(j).sprime2;    
    
    
gamma_dist=f_gamma_dist_const(Y,nbd,nd,body1,body2,sprime1,sprime2);

gamma_dr_d(j)=gamma_dist+2*dc(j)*dc_dotdot(j)+2*dc_dot(j)*dc_dot(j);

end

end