function [gamma_rev] = f_gamma_rev(nbd,Y,PHIparameters)

gamma_rev=zeros(30,1);

for k = 1:nbd
    body1 = PHIparameters.rev_constraint(k).body1;
    body2 = PHIparameters.rev_constraint(k).body2;
    
    sprime1 = PHIparameters.rev_constraint(k).sprime1;
    sprime2 = PHIparameters.rev_constraint(k).sprime2;
    
    gamma_sper =  f_gamma_spherical(nbd,Y,body1,body2,sprime1,sprime2);
    
    aj=[0; 0; 1];
    ai1=[1; 0; 0];
    ai2=[0; 1; 0];
    
    gamma_dot11 = f_gamma_dot1(nbd,body1,body2,Y,ai1,aj);
    gamma_dot12 = f_gamma_dot1(nbd,body1,body2,Y,ai2,aj);    
    
    gamma_rev_temp=[gamma_dot11;gamma_dot12;gamma_sper];
    gamma_rev(k*5-4:k*5) = gamma_rev_temp;
    
end

gamma_rev_1 = gamma_rev(1:10);
gamma_rev_2 = gamma_rev(13:26);
gamma_rev_3 = gamma_rev(28:30);

gamma_rev = [gamma_rev_1;gamma_rev_2;gamma_rev_3];


end

