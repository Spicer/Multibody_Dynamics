function [gamma_p] = f_gamma_p(n,Y)

[~,qpi,~,~,~,~] = f_StateVar(Y,n);
gamma_p = [];
for i = 1:n
    qp = qpi(4*i-3:4*i);
    gamma_p = [gamma_p;qp'*qp];
end
gamma_p = -2*gamma_p;
end
