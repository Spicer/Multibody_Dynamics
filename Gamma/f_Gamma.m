function [gamma] = f_Gamma(t,Y,nbd,nd,PHIparameters)

body1 = PHIparameters.sp_constraint(1).body1;
body2 = PHIparameters.sp_constraint(1).body2;

sprime1 = PHIparameters.sp_constraint(1).sprime1;
sprime2 = PHIparameters.sp_constraint(1).sprime2;

gamma_sper = f_gamma_spherical(nbd,Y,body1,body2,sprime1,sprime2);

gamma_rev = f_gamma_rev(nbd,Y,PHIparameters);

gamma_dr_a = [-0.25*cos(t/2); 0; -0.25*sin(t/2); 0;];

gamma_dr_d = f_gamma_dr_d(Y,nbd,nd,PHIparameters,t);

gamma_ep = zeros(5,1);

gamma = [gamma_rev;gamma_sper;gamma_dr_d;gamma_ep;gamma_dr_a];

end

