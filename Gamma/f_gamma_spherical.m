function[gamma_sperical]=f_gamma_spherical(nbd,Y,body1,body2,sprime1,sprime2)

[~,qp,~,~] = f_StateVar(Y,nbd);

if body1~=0 && body2~=0
[G1] = f_G(nbd,body1,Y);
[G2] = f_G(nbd,body2,Y);

p1_dot= qp(body1*4-3:body1*4);
p2_dot= qp(body2*4-3:body2*4);

omega1= 2*G1*p1_dot;
omega2= 2*G2*p2_dot;


omega1_t= f_SSM(omega1);
omega2_t= f_SSM(omega2);


[A1] = f_A(nbd,body1,Y);
[A2] = f_A(nbd,body2,Y);

elseif body1==0
    
[G2] = f_G(nbd,body2,Y);

p2_dot= qp(body2*4-3:body2*4);

omega2= 2*G2*p2_dot;

omega1_t= eye(3);
omega2_t= f_SSM(omega2);


[A1] = eye(3);
[A2] = f_A(nbd,body2,Y);

end

gamma_sperical= A1*omega1_t*omega1_t*sprime1 - A2*omega2_t*omega2_t*sprime2;

end