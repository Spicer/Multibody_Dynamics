function [PHI_p_dp] = f_PHIp_dp(nbd,Y)

[~,~,~,p_vec] = f_StateVar(Y,nbd);

n_p = 4*nbd;

PHI_p_dp = zeros(nbd-1,n_p);

for i = 2:nbd;
    PHI_p_dp(i-1,i*4-3:i*4) = p_vec(4*i-3:4*i)';
end

PHI_p_dp = 2*PHI_p_dp;
end

