function [Gamma_d1]=f_gamma_dot1(nbd,body1,body2,Y,a1,a2)


[~,qp,~,~]=f_StateVar(Y,nbd);

if body1~=0
    p_dot1=qp(body1*4-3:body1*4);
    
    G1=f_G(nbd,body1,Y);
    
    omega1=2*G1*p_dot1;
    omega_til1=f_SSM(omega1);
    
    A1=f_A(nbd,body1,Y);
end

A2=f_A(nbd,body1,Y);

p_dot2=qp(body2*4-3:body2*4);

G2=f_G(nbd,body2,Y);

omega2=2*G2*p_dot2;
omega_til2=f_SSM(omega2);

a_til1=f_SSM(a1);
a_til2=f_SSM(a2);

if body1~=0 && body2~=0 
    
   Gamma_d1=-a2'*(A2'*A1*omega_til1*omega_til1+omega_til2*omega_til2*A2'*A1)*a1+...
       2*omega2'*a_til2*A2'*A1*a_til1*omega1;
else
    if body1==0 
        Gamma_d1=-a2'*(omega_til2*omega_til2*A2')*a1;
    else
        error('Wrong Configuration')
    end
end
end