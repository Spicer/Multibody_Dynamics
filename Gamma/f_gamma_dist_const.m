function [gamma_dist]= f_gamma_dist_const(Y,nbd,nd,body1,body2,sprime1,sprime2)


[qr,qp,~,~] = f_StateVar(Y,nbd);


if body1~=0
    p_dot1=qp(body1*4-3:body1*4);
    
    G1=f_G(nbd,body1,Y);
    omega1=2*G1*p_dot1;
    omega1_til=f_SSM(omega1);
    A1=f_A(nbd,body1,Y);
    r_dot1=qr(body1*3-2:body1*3);
    sprime1_til=f_SSM(sprime1);
end

r_dot2=qr(body2*3-2:body2*3);
p_dot2=qp(body2*4-3:body2*4);

A2=f_A(nbd,body2,Y);
G2=f_G(nbd,body2,Y);
omega2=2*G2*p_dot2;
omega2_til=f_SSM(omega2);
sprime2_til=f_SSM(sprime2);

PHI_sp=f_Spherical(Y,nbd,nd,body1,body2,sprime1,sprime2);

if body1~=0 && body2~=0
    
    gamma_dist=-2*(r_dot2-r_dot1)'*(r_dot2-r_dot1)+2*sprime2'*omega2_til*omega2_til*sprime2+...
        2*sprime1'*omega1_til*omega1_til*sprime1-4*sprime2'*omega2_til*A2'*A1*omega1_til*sprime1+...
        4*(r_dot2-r_dot1)'*(A2*sprime2_til*omega2-A1*sprime1_til*omega1)-...
        2*PHI_sp'*(A1*omega1_til*sprime1_til*omega1-A2*omega1_til*sprime2_til*omega2);
else
    if body1==0 
        gamma_dist=-2*(r_dot2-r_dot1)'*(r_dot2-r_dot1)+2*Sprime2'*omega2_til*omega2_til*Sprime2+...
        4*(r_dot2)'*(A2*sprime2_til*omega2);
    else
        error('Wrong Configuration')
    end
end
end


