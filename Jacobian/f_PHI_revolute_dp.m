function PHI_revolute_dp = f_PHI_revolute_dp(nbd,Y,PHIparameters)

PHI_dp=zeros(30,18);

for k = 1:nbd
    
    body1 = PHIparameters.rev_constraint(k).body1;
    body2 = PHIparameters.rev_constraint(k).body2;
    
    sprime1 = PHIparameters.rev_constraint(k).sprime1;
    sprime2 = PHIparameters.rev_constraint(k).sprime2;
    
    PHI_sp_body1_dp =  f_PHI_spherical_dp(nbd,Y,body1,sprime1);
    PHI_sp_body2_dp = -f_PHI_spherical_dp(nbd,Y,body2,sprime2);
    
    fi=[1; 0; 0];
    gi=[0; 1; 0];
    hj=[0; 0; 1];
    
    PHI_dot11_body1_dp = f_PHI_d1_dp(nbd,body1,body2,Y,fi,hj,body1);
    PHI_dot11_body2_dp = f_PHI_d1_dp(nbd,body2,body1,Y,hj,fi,body2);
    
    PHI_dot12_body1_dp = f_PHI_d1_dp(nbd,body1,body2,Y,gi,hj,body1);    
    PHI_dot12_body2_dp = f_PHI_d1_dp(nbd,body2,body1,Y,hj,gi,body2);
    
    PHI_revolute1_dp = [PHI_dot11_body1_dp;PHI_dot12_body1_dp;PHI_sp_body1_dp];
    PHI_revolute2_dp = [PHI_dot11_body2_dp;PHI_dot12_body2_dp;PHI_sp_body2_dp];
    
    PHI_dp(k*5-4:k*5,body1*3-2:body1*3) = PHI_revolute1_dp;
    PHI_dp(k*5-4:k*5,body2*3-2:body2*3) = PHI_revolute2_dp;
    
end


PHI_p1 = PHI_dp(1:10,:);
PHI_p2 = PHI_dp(13:26,:);
PHI_p3 = PHI_dp(28:30,:);

PHI_dp = [PHI_p1;PHI_p2;PHI_p3];

G = f_GG(nbd,Y);
PHI_revolute_dp = 2*PHI_dp*G;

end

