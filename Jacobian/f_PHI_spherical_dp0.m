function [PHIp_sp] = f_PHI_spherical_dp0(nbd,Y,PHIparameters)


k=1;

PHI_spherical_dp=zeros(3,18);


 body1 = PHIparameters.sp_constraint(k).body1;
 body2 = PHIparameters.sp_constraint(k).body2;
    
 sprime1 = PHIparameters.sp_constraint(k).sprime1;
 sprime2 = PHIparameters.sp_constraint(k).sprime2;
 
 PHIp_sp_body1 =  f_PHI_spherical_dp(nbd,Y,body1,sprime1);
 PHIp_sp_body2 = -f_PHI_spherical_dp(nbd,Y,body2,sprime2);
 
if body1 ~=0
PHI_spherical_dp(:,body1*3-2:body1*3) = PHIp_sp_body1 ;
end
PHI_spherical_dp(:,body2*3-2:body2*3) = PHIp_sp_body2 ;

G = f_GG(nbd,Y);
PHIp_sp = 2*PHI_spherical_dp*G;


end

