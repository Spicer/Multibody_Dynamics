function [PHI_d1_dp] = f_PHI_d1_dp(nbd,body1,body2,Y,ai,aj,wrt)

Ait = f_SSM(ai);
Ajt = f_SSM(aj);

    Ai = f_A(nbd,body1,Y);
    Aj = f_A(nbd,body2,Y);
    
    if wrt==body1
    PHI_d1_dp = [-aj' * Aj' * Ai * Ait];
    
    elseif wrt== body2
    PHI_d1_dp = [-ai' * Ai' * Aj * Ajt];
    else 
        error('Please specify a ccorrect body number for wrt');
    end
        
   



end

