function [PHI_SS_dp] = f_PHI_SS_dp(Y,nbd,nd,PHIparameters)

PHIw_dr_d = zeros(3,18);

for i = 1:nd
    body1 = PHIparameters.dr_constraint(i).body1;
    body2 = PHIparameters.dr_constraint(i).body2;
    
    sprime1 = PHIparameters.dr_constraint(i).sprime1;
    sprime2 = PHIparameters.dr_constraint(i).sprime2;
    
    sprime1_t = f_SSM(sprime1);
    sprime2_t = f_SSM(sprime2);
    
    [~,~,r,~] = f_StateVar(Y,nbd);
    
    Ai = f_A(nbd,body1,Y);
    Aj = f_A(nbd,body2,Y);
    ri = r(body1*3-2:body1*3);
    rj = r(body2*3-2:body2*3);
 
    d = rj + Aj*sprime2 - ri - Ai*sprime1;

    PHIp_dr_d_b1 = 2*transpose(d)*Ai*sprime1_t;
    PHIp_dr_d_b2 = -2*transpose(d)*Aj*sprime2_t;
    PHIw_dr_d(i,body1*3-2:body1*3) = PHIp_dr_d_b1;    
    PHIw_dr_d(i,body2*3-2:body2*3) = PHIp_dr_d_b2;   
end

G = f_GG(nbd,Y);

PHI_SS_dp = 2*PHIw_dr_d*G;
end

