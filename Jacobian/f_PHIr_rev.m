function [PHI_r] = f_PHIr_rev(nbd,PHIparameters)

PHI_r = zeros(30,18);

for i=1:nbd;
    body1 = PHIparameters.rev_constraint(i).body1;
    body2 = PHIparameters.rev_constraint(i).body2;
    PHI_r(i*5-2:i*5,body1*3-2:body1*3) = -eye(3);
    PHI_r(i*5-2:i*5,body2*3-2:body2*3) = eye(3);
end

PHI_r1 = PHI_r(1:10,:);

PHI_r2 = PHI_r(13:26,:);

PHI_r3 = PHI_r(28:30,:);

PHI_r = [PHI_r1;PHI_r2;PHI_r3];

end

