function [PHIq] = jacobian(Y,nbd,nd,PHIparameters)

PHI_spherical_dr = [eye(3),zeros(3,15)];

PHI_revolute_dr = f_PHIr_rev(nbd,PHIparameters);

PHI_angular_dr = zeros(3,18);

PHI_SS_dr = f_PHI_SS(Y,nbd,nd,PHIparameters);

PHI_drivingangle_dr = zeros(6,18);

PHI_dr = [PHI_revolute_dr;PHI_spherical_dr;PHI_SS_dr;PHI_drivingangle_dr;PHI_angular_dr];

PHI_spherical_dp = f_PHI_spherical_dp0(nbd,Y,PHIparameters);
PHI_revolute_dp= f_PHI_revolute_dp(nbd,Y,PHIparameters);

PHI_drivingangle_p1 = [1 0 0 0 zeros(1,20)];
PHI_drivingangle_p2 = [0 1 0 0 zeros(1,20)];
PHI_drivingangle_p3 = [0 0 1 0 zeros(1,20)];
PHI_drivingangle_p4 = [0 0 0 1 zeros(1,20)];

PHI_drivingangle_p = [PHI_drivingangle_p1;PHI_drivingangle_p2;PHI_drivingangle_p3;PHI_drivingangle_p4];
PHI_driving_dp = f_PHI_SS_dp(Y,nbd,nd,PHIparameters);

PHI_p_dp = f_PHIp_dp(nbd,Y);

PHI_dp= [PHI_revolute_dp;PHI_spherical_dp;PHI_driving_dp;PHI_p_dp;PHI_drivingangle_p];


PHIq=zeros(42,42);

for i=1:nbd
    
PHIq(:,i*7-6:i*7) = [PHI_dr(:,i*3-2:i*3),PHI_dp(:,i*4-3:i*4)];

end

end

