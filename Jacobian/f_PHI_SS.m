function [PHI_SS] = f_PHI_SS(Y,nbd,nd,PHIparameters)

PHI_SS = zeros(3,18);

for i = 1:nd
    
    body1 = PHIparameters.dr_constraint(i).body1;
    body2 = PHIparameters.dr_constraint(i).body2;
    sprime1 = PHIparameters.dr_constraint(i).sprime1;
    sprime2 = PHIparameters.dr_constraint(i).sprime2;
    
    [~,~,r,~] = f_StateVar(Y,nbd);
    
    Ai = f_A(nbd,body1,Y);
    Aj = f_A(nbd,body2,Y);
    ri = r(body1*3-2:body1*3);
    rj = r(body2*3-2:body2*3);
  
    d = rj + Aj*sprime2 - ri - Ai*sprime1;
    
    PHI_SS(i,body1*3-2:body1*3) = -2*transpose(d);
    PHI_SS(i,body2*3-2:body2*3) = 2*transpose(d);

end

end

