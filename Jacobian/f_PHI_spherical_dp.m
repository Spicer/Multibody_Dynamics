function [PHI_spherical_dp1] = f_PHI_spherical_dp(nbd,Y,body,sprime)

PHI_spherical_dp1=zeros(3,3);

sprime_til = f_SSM(sprime);

if body~=0 
    Ai = f_A(nbd,body,Y);
    PHI_spherical_dp1 = [Ai*sprime_til];

end

end

